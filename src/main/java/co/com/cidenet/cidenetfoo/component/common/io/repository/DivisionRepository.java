package co.com.cidenet.cidenetfoo.component.common.io.repository;

import co.com.cidenet.cidenetfoo.component.common.model.Division;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DivisionRepository extends JpaRepository<Division, Long> {
}
