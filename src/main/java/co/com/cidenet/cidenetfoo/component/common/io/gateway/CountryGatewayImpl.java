package co.com.cidenet.cidenetfoo.component.common.io.gateway;

import co.com.cidenet.cidenetfoo.component.common.io.repository.CountryRepository;
import co.com.cidenet.cidenetfoo.component.common.model.Country;
import co.com.cidenet.cidenetfoo.component.common.service.CountryGateway;
import co.com.cidenet.cidenetfoo.component.shared.web.exception.ResourceNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@RequiredArgsConstructor
public class CountryGatewayImpl implements CountryGateway {

    private final CountryRepository countryRepository;

    private static final String RESOURCE_NOT_FOUND = "Country not found";

    @Override
    public Country findById(Long id) {

        Country countryFound = countryRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(RESOURCE_NOT_FOUND));

        return countryFound;

    }

    @Override
    public List<Country> findAll() {

        List<Country> countriesFound = countryRepository.findAll();

        return countriesFound;
    }
}
