package co.com.cidenet.cidenetfoo.component.user.io.repository;

import co.com.cidenet.cidenetfoo.component.user.model.IdentificationType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IdentificationTypeRepository extends JpaRepository<IdentificationType, Long> {
}
