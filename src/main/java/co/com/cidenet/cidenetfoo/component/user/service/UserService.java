package co.com.cidenet.cidenetfoo.component.user.service;

import co.com.cidenet.cidenetfoo.component.user.model.IdentificationType;
import co.com.cidenet.cidenetfoo.component.user.model.User;
import co.com.cidenet.cidenetfoo.component.user.service.model.UserQuerySearchCmd;
import co.com.cidenet.cidenetfoo.component.user.service.model.UserSaveCmd;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.validation.constraints.NotNull;
import java.util.List;

public interface UserService {

    User create(@NotNull UserSaveCmd userToCreateCmd);

    User findById(@NotNull Long id);

    User update(@NotNull Long id, @NotNull UserSaveCmd userToUpdateCmd);
    
    void deleteById(@NotNull Long id);

    Page<User> findByParameters(@NotNull UserQuerySearchCmd queryCriteriaCmd, @NotNull Pageable pageable);

    List<IdentificationType> findAllIdentificationTypes();
}
