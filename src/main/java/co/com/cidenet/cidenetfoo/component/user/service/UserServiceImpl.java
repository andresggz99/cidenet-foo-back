package co.com.cidenet.cidenetfoo.component.user.service;

import co.com.cidenet.cidenetfoo.component.common.model.Country;
import co.com.cidenet.cidenetfoo.component.common.model.Division;
import co.com.cidenet.cidenetfoo.component.common.service.CountryService;
import co.com.cidenet.cidenetfoo.component.common.service.DivisionService;
import co.com.cidenet.cidenetfoo.component.shared.web.exception.BusinessException;
import co.com.cidenet.cidenetfoo.component.user.model.IdentificationType;
import co.com.cidenet.cidenetfoo.component.user.model.User;
import co.com.cidenet.cidenetfoo.component.user.service.model.UserQuerySearchCmd;
import co.com.cidenet.cidenetfoo.component.user.service.model.UserSaveCmd;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import javax.validation.Validator;
import java.time.LocalDate;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import static java.util.Objects.nonNull;

@Service
@RequiredArgsConstructor
@Transactional
public class UserServiceImpl implements UserService{

    public static final String EMAIL_FORMAT_WITH_CONSECUTIVE = "%s.%s%s@%s";
    public static final String EMAIL_FORMAT_WITHOUT_CONSECUTIVE = "%s.%s@%s";
    public static final String ENROLLMENT_DATE_CANT_BE_IN_THE_FUTURE = "Enrollment date can't be in the future";
    public static final String ENROLLMENT_DATE_CAN_ONLY_BE_UP_TO_1_MONTH_LESS_THAN_THE_CURRENT_DATE = "Enrollment date can only be up to 1 month less than the current date";
    public static final int FIRST_CONSECUTIVE_EMAILS = 1;

    private final UserGateway userGateway;
    private final CountryService countryService;
    private final DivisionService divisionService;

    private final Validator validator;

    @Override
    public User create(UserSaveCmd userToCreateCmd) {

        Set<ConstraintViolation<UserSaveCmd>> violations = validator.validate(userToCreateCmd);
        if (!violations.isEmpty()) {
            throw new ConstraintViolationException(violations);
        }

        enrolmentDateIsValid(userToCreateCmd.getEnrollmentDate());

        Country countryInDataBase = countryService.findById(userToCreateCmd.getLaborCountryId());

        IdentificationType identificationTypeInDataBase = userGateway.findIdentificationTypeById(userToCreateCmd.getIdentificationTypeId());

        Division divisionInDataBase = divisionService.findById(userToCreateCmd.getDivisionId());

        String emailGenerated = getNewEmail(userToCreateCmd.getFirstName(), userToCreateCmd.getPrimaryLastName(), countryInDataBase.getEmailDomain());

        User userToCreate = UserSaveCmd.toModel(userToCreateCmd)
                .toBuilder().laborCountry(countryInDataBase).identificationType(identificationTypeInDataBase).email(emailGenerated)
                .division(divisionInDataBase).build();

        User userCreated = userGateway.save(userToCreate);

        return userCreated;
    }

    private void enrolmentDateIsValid(LocalDate enrollmentDate) {

        LocalDate currentDate = LocalDate.now();

        if(enrollmentDate.isAfter(currentDate)){
            throw new BusinessException(ENROLLMENT_DATE_CANT_BE_IN_THE_FUTURE);
        }

        if(enrollmentDate.plusMonths(1).isBefore(currentDate)){
            throw new BusinessException(ENROLLMENT_DATE_CAN_ONLY_BE_UP_TO_1_MONTH_LESS_THAN_THE_CURRENT_DATE);
        }
    }

    private String getNewEmail(String firstName, String primaryLastName, String emailDomain) {

        String primaryLastNameWithoutBlankSpaces = primaryLastName.replace(" ", "");

        String emailWithoutConsecutive = String.format(EMAIL_FORMAT_WITHOUT_CONSECUTIVE, firstName, primaryLastNameWithoutBlankSpaces, emailDomain);

        Long biggestConsecutiveUsed = userGateway.getBiggestConsecutiveUsedInEmail(emailWithoutConsecutive).orElse(null);

        String newEmail;

        if(nonNull(biggestConsecutiveUsed)){
            Long nextConsecutive = biggestConsecutiveUsed + 1;
            newEmail = String.format(EMAIL_FORMAT_WITH_CONSECUTIVE, firstName, primaryLastNameWithoutBlankSpaces, nextConsecutive, emailDomain);
        }else{
            if(userGateway.existsByEmail(emailWithoutConsecutive)){
                newEmail = String.format(EMAIL_FORMAT_WITH_CONSECUTIVE, firstName, primaryLastNameWithoutBlankSpaces, FIRST_CONSECUTIVE_EMAILS, emailDomain);
            }else{
                newEmail = emailWithoutConsecutive;
            }
        }

        return newEmail.toLowerCase(Locale.ENGLISH);
    }

    @Override
    @Transactional(readOnly = true)
    public User findById(Long id) {

        User userFound = userGateway.findById(id);

        return userFound;
    }

    @Override
    public User update(Long id, UserSaveCmd userToUpdateCmd) {

        Set<ConstraintViolation<UserSaveCmd>> violations = validator.validate(userToUpdateCmd);
        if (!violations.isEmpty()) {
            throw new ConstraintViolationException(violations);
        }

        User userInDataBase = findById(id);

        Country countryInDataBase = countryService.findById(userToUpdateCmd.getLaborCountryId());

        IdentificationType identificationTypeInDataBase = userGateway.findIdentificationTypeById(userToUpdateCmd.getIdentificationTypeId());

        Division divisionInDataBase = divisionService.findById(userToUpdateCmd.getDivisionId());

        User userToUpdate = userInDataBase.toBuilder().firstName(userToUpdateCmd.getFirstName())
                .middleName(userToUpdateCmd.getMiddleName()).primaryLastName(userToUpdateCmd.getPrimaryLastName()).secondLastName(userToUpdateCmd.getSecondLastName())
                .identificationType(identificationTypeInDataBase).identification(userToUpdateCmd.getIdentification())
                .enrollmentDate(userToUpdateCmd.getEnrollmentDate())
                .laborCountry(countryInDataBase).division(divisionInDataBase).build();

        if(firstNameOrPrimaryLastNameChanged(userInDataBase, userToUpdateCmd)){
            String emailGenerated = getNewEmail(userToUpdateCmd.getFirstName(), userToUpdateCmd.getPrimaryLastName(), countryInDataBase.getEmailDomain());
            userToUpdate.setEmail(emailGenerated);
        }


        User userUpdated = userGateway.update(id, userToUpdate);

        return userUpdated;

    }

    private boolean firstNameOrPrimaryLastNameChanged(User userInDataBase, UserSaveCmd userToUpdateCmd) {
        return !userInDataBase.getFirstName().equalsIgnoreCase(userToUpdateCmd.getFirstName()) ||
                !userInDataBase.getPrimaryLastName().equalsIgnoreCase(userToUpdateCmd.getPrimaryLastName());
    }

    @Override
    public void deleteById(Long id) {

        userGateway.deleteById(id);

    }

    @Override
    @Transactional(readOnly = true)
    public Page<User> findByParameters(UserQuerySearchCmd queryCriteriaCmd, Pageable pageable) {

        Page<User> usersFound = userGateway.findByParameters(queryCriteriaCmd, pageable);

        return usersFound;

    }

    @Override
    public List<IdentificationType> findAllIdentificationTypes() {

        List<IdentificationType> identificationTypesFound  = userGateway.findAllIdentificationTypes();

        return identificationTypesFound;
    }
}
