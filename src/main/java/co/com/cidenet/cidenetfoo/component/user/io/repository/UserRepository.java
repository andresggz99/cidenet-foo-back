package co.com.cidenet.cidenetfoo.component.user.io.repository;

import co.com.cidenet.cidenetfoo.component.user.model.User;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends PagingAndSortingRepository<User, Long>, JpaSpecificationExecutor<User> {

    @Query(value = "SELECT MAX(REGEXP_REPLACE(original_email, '\\\\D', '')) biggest_consecutive FROM\n" +
            "(SELECT original_email FROM (\n" +
            "SELECT email original_email, REGEXP_REPLACE(email, '[0-9]', '') email_without_consecutives\n" +
            "FROM users) emails\n" +
            "WHERE email_without_consecutives = ?1) filtered_emails", nativeQuery = true)
    Optional<Long> getBiggestConsecutiveUsedInEmail(String emailWithoutNumbers);

    Boolean existsByEmail(String email);
}
