package co.com.cidenet.cidenetfoo.component.user.model;

import co.com.cidenet.cidenetfoo.component.common.model.Country;
import co.com.cidenet.cidenetfoo.component.common.model.Division;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Table(name = "users")
@Data
@Generated
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @EqualsAndHashCode.Include
    private Long id;

    @NotNull
    @Column(name = "identification", nullable = false)
    @NotBlank
    @Size(max = 20)
    private String identification;

    @ManyToOne
    @JoinColumn(name = "identification_type_id")
    private IdentificationType identificationType;

    @ManyToOne
    @JoinColumn(name = "division_id")
    private Division division;

    @ManyToOne
    @JoinColumn(name = "labor_country_id")
    private Country laborCountry;

    @NotNull
    @Column(name = "first_name", nullable = false)
    @NotBlank
    @Size(max = 20)
    private String firstName;

    @Column(name = "middle_name")
    @Size(max = 50)
    private String middleName;

    @NotNull
    @Column(name = "primary_last_name", nullable = false)
    @NotBlank
    @Size(max = 20)
    private String primaryLastName;

    @NotNull
    @Column(name = "second_last_name", nullable = false)
    @NotBlank
    @Size(max = 20)
    private String secondLastName;

    @NotNull
    @Column(name = "email", nullable = false, unique = true)
    @Size(max = 300)
    private String email;

    @Column(name = "enrollment_date")
    private LocalDate enrollmentDate;

    @Column(name = "created_at")
    private LocalDateTime createDate;

    @Column(name = "updated_at")
    private LocalDateTime updateDate;




}
