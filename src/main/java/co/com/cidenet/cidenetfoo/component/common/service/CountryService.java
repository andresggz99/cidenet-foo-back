package co.com.cidenet.cidenetfoo.component.common.service;

import co.com.cidenet.cidenetfoo.component.common.model.Country;

import javax.validation.constraints.NotNull;
import java.util.List;

public interface CountryService {

    Country findById(@NotNull Long id);

    List<Country> findAll();
}
