package co.com.cidenet.cidenetfoo.component.user.io.web.v1.model;

import co.com.cidenet.cidenetfoo.component.shared.constants.ValidatorConstants;
import co.com.cidenet.cidenetfoo.component.user.service.model.UserSaveCmd;
import lombok.*;

import javax.validation.constraints.*;
import java.time.LocalDate;

@Data
@Generated
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserSaveRequest {

    @NotNull
    @NotBlank
    @Size(max = ValidatorConstants.MAX_SIZE_IDENTIFICATION)
    @Pattern(regexp = ValidatorConstants.REGEX_IDENTIFICATION)
    private String identification;

    @NotNull
    private Long identificationTypeId;

    @NotNull
    private Long divisionId;

    @NotNull
    private Long laborCountryId;

    @NotNull
    @NotBlank
    @Size(max = ValidatorConstants.MAX_SIZE_FIRST_NAME)
    @Pattern(regexp = ValidatorConstants.REGEX_FIRST_NAME)
    private String firstName;

    @Size(max = ValidatorConstants.MAX_SIZE_MIDDLE_NAME)
    @Pattern(regexp = ValidatorConstants.REGEX_MIDDLE_NAME)
    private String middleName;

    @NotNull
    @NotBlank
    @Size(max = ValidatorConstants.MAX_SIZE_PRIMARY_LAST_NAME)
    @Pattern(regexp = ValidatorConstants.REGEX_PRIMARY_LAST_NAME)
    private String primaryLastName;

    @NotNull
    @NotBlank
    @Size(max = ValidatorConstants.MAX_SIZE_SECOND_LAST_NAME)
    @Pattern(regexp = ValidatorConstants.REGEX_SECOND_LAST_NAME)
    private String secondLastName;

    @NotNull
    private LocalDate enrollmentDate;

    public static UserSaveCmd toModel(@NotNull UserSaveRequest userToCreateRequest){
        return UserSaveCmd.builder().firstName(userToCreateRequest.getFirstName())
                .middleName(userToCreateRequest.getMiddleName())
                .primaryLastName(userToCreateRequest.getPrimaryLastName())
                .secondLastName(userToCreateRequest.getSecondLastName())
                .identification(userToCreateRequest.getIdentification())
                .divisionId(userToCreateRequest.getDivisionId())
                .enrollmentDate(userToCreateRequest.getEnrollmentDate())
                .identificationTypeId(userToCreateRequest.getIdentificationTypeId())
                .laborCountryId(userToCreateRequest.getLaborCountryId())
                .build();
    }

}
