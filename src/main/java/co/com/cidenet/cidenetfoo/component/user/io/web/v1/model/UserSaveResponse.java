package co.com.cidenet.cidenetfoo.component.user.io.web.v1.model;

import co.com.cidenet.cidenetfoo.component.user.model.User;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@Generated
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserSaveResponse {

    private Long id;

    private String identification;

    private IdentificationTypeSaveResponse identificationType;

    private CountrySaveResponse laborCountry;

    private DivisionSaveResponse division;

    private String firstName;

    private String middleName;

    private String primaryLastName;

    private String secondLastName;

    private String email;

    private LocalDateTime createDate;

    private LocalDateTime updateDate;

    private LocalDate enrollmentDate;

    public static UserSaveResponse fromModel(User user){
        return UserSaveResponse.builder().id(user.getId()).identification(user.getIdentification())
                .firstName(user.getFirstName()).middleName(user.getMiddleName()).primaryLastName(user.getPrimaryLastName())
                .secondLastName(user.getSecondLastName()).email(user.getEmail()).createDate(user.getCreateDate())
                .updateDate(user.getUpdateDate())
                .enrollmentDate(user.getEnrollmentDate())
                .identificationType(IdentificationTypeSaveResponse.builder().id(user.getIdentificationType().getId()).name(user.getIdentificationType().getName()).build())
                .laborCountry(CountrySaveResponse.builder().id(user.getLaborCountry().getId()).name(user.getLaborCountry().getName()).build())
                .division(DivisionSaveResponse.builder().id(user.getId()).name(user.getDivision().getName()).build())
                .build();
    }
}
