package co.com.cidenet.cidenetfoo.component.user.service.model;

import co.com.cidenet.cidenetfoo.component.shared.constants.ValidatorConstants;
import co.com.cidenet.cidenetfoo.component.user.model.User;
import lombok.*;

import javax.validation.constraints.*;
import java.time.LocalDate;

@Data
@Generated
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserSaveCmd {

    @NotNull
    @NotBlank
    @Size(max = ValidatorConstants.MAX_SIZE_IDENTIFICATION)
    @Pattern(regexp = ValidatorConstants.REGEX_IDENTIFICATION)
    private String identification;

    @NotNull
    private Long identificationTypeId;

    @NotNull
    private Long divisionId;

    @NotNull
    private Long laborCountryId;

    @NotNull
    @NotBlank
    @Size(max = ValidatorConstants.MAX_SIZE_FIRST_NAME)
    @Pattern(regexp = ValidatorConstants.REGEX_FIRST_NAME)
    private String firstName;

    @Size(max = ValidatorConstants.MAX_SIZE_MIDDLE_NAME)
    @Pattern(regexp = ValidatorConstants.REGEX_MIDDLE_NAME)
    private String middleName;

    @NotNull
    @NotBlank
    @Size(max = ValidatorConstants.MAX_SIZE_PRIMARY_LAST_NAME)
    @Pattern(regexp = ValidatorConstants.REGEX_PRIMARY_LAST_NAME)
    private String primaryLastName;

    @NotNull
    @NotBlank
    @Size(max = ValidatorConstants.MAX_SIZE_SECOND_LAST_NAME)
    @Pattern(regexp = ValidatorConstants.REGEX_SECOND_LAST_NAME)
    private String secondLastName;

    @NotNull
    private LocalDate enrollmentDate;

    public static User toModel(@NotNull UserSaveCmd userToCreateCmd){
        return User.builder().firstName(userToCreateCmd.getFirstName())
                .middleName(userToCreateCmd.getMiddleName())
                .primaryLastName(userToCreateCmd.getPrimaryLastName())
                .secondLastName(userToCreateCmd.getSecondLastName())
                .identification(userToCreateCmd.getIdentification())
                .enrollmentDate(userToCreateCmd.getEnrollmentDate())
                .build();
    }

}
