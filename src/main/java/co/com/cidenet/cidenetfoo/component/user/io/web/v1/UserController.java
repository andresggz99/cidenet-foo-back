package co.com.cidenet.cidenetfoo.component.user.io.web.v1;

import co.com.cidenet.cidenetfoo.component.shared.model.ResponsePagination;
import co.com.cidenet.cidenetfoo.component.user.io.web.v1.model.UserListResponse;
import co.com.cidenet.cidenetfoo.component.user.io.web.v1.model.UserQuerySearchRequest;
import co.com.cidenet.cidenetfoo.component.user.io.web.v1.model.UserSaveRequest;
import co.com.cidenet.cidenetfoo.component.user.io.web.v1.model.UserSaveResponse;
import co.com.cidenet.cidenetfoo.component.user.model.User;
import co.com.cidenet.cidenetfoo.component.user.service.UserService;
import co.com.cidenet.cidenetfoo.component.user.service.model.UserQuerySearchCmd;
import co.com.cidenet.cidenetfoo.component.user.service.model.UserSaveCmd;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.web.util.UriComponentsBuilder.fromUriString;

@RestController
@RequestMapping(path = "/api/v1/users", produces = MediaType.APPLICATION_JSON_VALUE)
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @PostMapping
    @ResponseStatus(value = HttpStatus.CREATED)
    @CrossOrigin(exposedHeaders = {HttpHeaders.LOCATION})
    public ResponseEntity<Void> create(@Valid @NotNull @RequestBody UserSaveRequest userToCreate) {

        UserSaveCmd userToCreateCmd = UserSaveRequest.toModel(userToCreate);

        User userCreated = userService.create(userToCreateCmd);

        URI location = fromUriString("/api/v1/users").path("/{id}")
                .buildAndExpand(userCreated.getId()).toUri();

        return ResponseEntity.created(location).build();
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<UserSaveResponse> findById(@Valid @PathVariable("id") @NotNull Long id) {

        User userFound = userService.findById(id);

        return ResponseEntity.ok(UserSaveResponse.fromModel(userFound));
    }

    @GetMapping
    public ResponsePagination<UserListResponse> findByParameters(@Valid @NotNull UserQuerySearchRequest queryCriteria,
                                                                 @PageableDefault(page = 0, size = 10,
                                                                         direction = Sort.Direction.DESC, sort = "id")
                                                                         Pageable pageable) {

        UserQuerySearchCmd queryCriteriaCmd = UserQuerySearchRequest.toModel(queryCriteria);

        Page<User> usersFound = userService.findByParameters(queryCriteriaCmd, pageable);
        List<UserListResponse> usersFoundList = usersFound.stream().map(UserListResponse::fromModel)
                .collect(Collectors.toList());

        return ResponsePagination.fromObject(usersFoundList, usersFound.getTotalElements(), usersFound.getNumber(),
                usersFoundList.size());
    }

    @DeleteMapping(path = "/{id}")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public ResponseEntity<Void> delete(@Valid @PathVariable("id") @NotNull Long id) {

        userService.deleteById(id);

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PutMapping(path = "/{id}")
    public ResponseEntity<UserSaveResponse> update(@Valid @RequestBody @NotNull UserSaveRequest userToUpdate,
                                                   @Valid @PathVariable("id") @NotNull Long id) {

        UserSaveCmd userToUpdateCmd = UserSaveRequest.toModel(userToUpdate);

        User userUpdated = userService.update(id, userToUpdateCmd);

        return ResponseEntity.ok(UserSaveResponse.fromModel(userUpdated));
    }
}
