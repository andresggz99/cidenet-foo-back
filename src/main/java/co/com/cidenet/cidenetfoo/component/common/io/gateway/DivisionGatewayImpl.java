package co.com.cidenet.cidenetfoo.component.common.io.gateway;

import co.com.cidenet.cidenetfoo.component.common.io.repository.DivisionRepository;
import co.com.cidenet.cidenetfoo.component.common.model.Division;
import co.com.cidenet.cidenetfoo.component.common.service.DivisionGateway;
import co.com.cidenet.cidenetfoo.component.shared.web.exception.ResourceNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@RequiredArgsConstructor
public class DivisionGatewayImpl implements DivisionGateway {

    private final DivisionRepository divisionRepository;

    private static final String RESOURCE_NOT_FOUND = "Division not found";


    @Override
    public List<Division> findAll() {

        List<Division> divisionsFound = divisionRepository.findAll();

        return divisionsFound;
    }

    @Override
    public Division findById(Long id) {

        Division divisionFound = divisionRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(RESOURCE_NOT_FOUND));

        return divisionFound;
    }
}
