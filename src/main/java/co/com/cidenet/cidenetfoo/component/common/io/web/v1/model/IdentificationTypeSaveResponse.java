package co.com.cidenet.cidenetfoo.component.common.io.web.v1.model;

import co.com.cidenet.cidenetfoo.component.user.model.IdentificationType;
import lombok.*;

@Data
@Generated
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class IdentificationTypeSaveResponse {

    private Long id;

    private String name;

    public static IdentificationTypeSaveResponse fromModel(IdentificationType  identificationType){
        return IdentificationTypeSaveResponse.builder().id(identificationType.getId())
                .name(identificationType.getName()).build();
    }

}
