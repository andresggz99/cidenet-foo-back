package co.com.cidenet.cidenetfoo.component.user.io.web.v1.model;

import co.com.cidenet.cidenetfoo.component.user.service.model.UserQuerySearchCmd;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

@Data
@Generated
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserQuerySearchRequest {

    private Long id;

    private String identification;

    private Long identificationTypeId;

    private Long laborCountryId;

    private Long divisionId;

    private String firstName;

    private String middleName;

    private String primaryLastName;

    private String secondLastName;

    private String email;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate enrollmentDate;

    public static UserQuerySearchCmd toModel(UserQuerySearchRequest queryCriteria){
        return UserQuerySearchCmd.builder()
                .id(queryCriteria.getId())
                .identification(queryCriteria.getIdentification())
                .identificationTypeId(queryCriteria.getIdentificationTypeId())
                .firstName(queryCriteria.getFirstName())
                .middleName(queryCriteria.getMiddleName())
                .primaryLastName(queryCriteria.getPrimaryLastName())
                .secondLastName(queryCriteria.getSecondLastName())
                .laborCountryId(queryCriteria.getLaborCountryId())
                .divisionId(queryCriteria.getDivisionId())
                .email(queryCriteria.getEmail())
                .enrollmentDate(queryCriteria.getEnrollmentDate())
                .build();
    }

}
