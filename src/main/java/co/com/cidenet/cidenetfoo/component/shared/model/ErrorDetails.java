package co.com.cidenet.cidenetfoo.component.shared.model;

import lombok.Generated;
import lombok.Getter;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Generated
public class ErrorDetails {

    private LocalDateTime timestamp;
    private String message;
    private String details;
    private String type;

    public ErrorDetails(LocalDateTime timestamp, String message, String details, String type) {
        super();
        this.timestamp = timestamp;
        this.message = message;
        this.details = details;
        this.type = type;
    }

}
