package co.com.cidenet.cidenetfoo.component.common.service;

import co.com.cidenet.cidenetfoo.component.common.model.Country;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CountryServiceImpl implements CountryService{

    private final CountryGateway countryGateway;


    @Override
    public Country findById(Long id) {

        Country countryFound = countryGateway.findById(id);

        return countryFound;
    }

    @Override
    public List<Country> findAll() {

        List<Country> countriesFound = countryGateway.findAll();

        return countriesFound;
    }
}
