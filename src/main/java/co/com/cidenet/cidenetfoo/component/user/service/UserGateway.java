package co.com.cidenet.cidenetfoo.component.user.service;

import co.com.cidenet.cidenetfoo.component.user.model.IdentificationType;
import co.com.cidenet.cidenetfoo.component.user.model.User;
import co.com.cidenet.cidenetfoo.component.user.service.model.UserQuerySearchCmd;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

public interface UserGateway {

    User save(@NotNull User userToCreate);

    User findById(@NotNull Long id);

    IdentificationType findIdentificationTypeById(@NotNull Long id);

    Optional<Long> getBiggestConsecutiveUsedInEmail(@NotNull String email);

    void deleteById(@NotNull Long id);

    User update(@NotNull Long id, @NotNull User userToUpdate);

    Page<User> findByParameters(@NotNull UserQuerySearchCmd queryCriteria, @NotNull Pageable pageable);

    List<IdentificationType> findAllIdentificationTypes();

    Boolean existsByEmail(@NotNull String email);
}
