package co.com.cidenet.cidenetfoo.component.common.service;

import co.com.cidenet.cidenetfoo.component.common.model.Division;

import javax.validation.constraints.NotNull;
import java.util.List;

public interface DivisionService {

    List<Division> findAll();

    Division findById(@NotNull Long id);
}
