package co.com.cidenet.cidenetfoo.component.shared.constants;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class ValidatorConstants {

    public static final String REGEX_FIRST_NAME = "[A-Z]+";
    public static final String REGEX_MIDDLE_NAME = "[A-Z\\s]+";
    public static final String REGEX_PRIMARY_LAST_NAME = "[A-Z\\s]+";
    public static final String REGEX_SECOND_LAST_NAME = "[A-Z\\s]+";
    public static final String REGEX_IDENTIFICATION = "[A-Za-z0-9-]+";
    public static final int MAX_SIZE_FIRST_NAME = 20;
    public static final int MAX_SIZE_MIDDLE_NAME = 50;
    public static final int MAX_SIZE_PRIMARY_LAST_NAME = 20;
    public static final int MAX_SIZE_SECOND_LAST_NAME = 20;
    public static final int MAX_SIZE_IDENTIFICATION = 20;

}
