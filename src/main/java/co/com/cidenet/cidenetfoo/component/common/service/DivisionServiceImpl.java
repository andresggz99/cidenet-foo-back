package co.com.cidenet.cidenetfoo.component.common.service;

import co.com.cidenet.cidenetfoo.component.common.model.Division;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class DivisionServiceImpl implements DivisionService{

    private final DivisionGateway divisionGateway;

    @Override
    public List<Division> findAll() {

        List<Division> divisionsFound = divisionGateway.findAll();

        return divisionsFound;
    }

    @Override
    public Division findById(Long id) {

        Division divisionFound = divisionGateway.findById(id);

        return divisionFound;
    }
}
