package co.com.cidenet.cidenetfoo.component.common.io.repository;

import co.com.cidenet.cidenetfoo.component.common.model.Country;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

public interface CountryRepository extends JpaRepository<Country, Long> {
}
