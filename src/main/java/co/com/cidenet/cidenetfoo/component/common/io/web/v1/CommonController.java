package co.com.cidenet.cidenetfoo.component.common.io.web.v1;

import co.com.cidenet.cidenetfoo.component.common.io.web.v1.model.CountrySaveResponse;
import co.com.cidenet.cidenetfoo.component.common.io.web.v1.model.DivisionSaveResponse;
import co.com.cidenet.cidenetfoo.component.common.io.web.v1.model.IdentificationTypeSaveResponse;
import co.com.cidenet.cidenetfoo.component.common.model.Country;
import co.com.cidenet.cidenetfoo.component.common.model.Division;
import co.com.cidenet.cidenetfoo.component.common.service.CountryService;
import co.com.cidenet.cidenetfoo.component.common.service.DivisionService;
import co.com.cidenet.cidenetfoo.component.user.model.IdentificationType;
import co.com.cidenet.cidenetfoo.component.user.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "/api/v1", produces = MediaType.APPLICATION_JSON_VALUE)
@RequiredArgsConstructor
public class CommonController {

    private final CountryService countryService;

    private final DivisionService divisionService;

    private final UserService userService;

    @GetMapping(path = "/divisions")
    public ResponseEntity<List<DivisionSaveResponse>> findAllDivisions(){

        List<Division> divisionsFound = divisionService.findAll();

        List<DivisionSaveResponse> divisionsFoundList = divisionsFound.stream().map(DivisionSaveResponse::fromModel)
                .collect(Collectors.toList());

        return ResponseEntity.ok(divisionsFoundList);
    }

    @GetMapping(path = "/divisions/{id}")
    public ResponseEntity<DivisionSaveResponse> findDivisionById(@Valid @NotNull @PathVariable("id") Long id){

        Division divisionFound = divisionService.findById(id);

        return ResponseEntity.ok(DivisionSaveResponse.fromModel(divisionFound));
    }

    @GetMapping(path = "/countries")
    public ResponseEntity<List<CountrySaveResponse>> findAllCountries(){

        List<Country> countriesFound = countryService.findAll();

        List<CountrySaveResponse> countriesFoundList = countriesFound.stream().map(CountrySaveResponse::fromModel)
                .collect(Collectors.toList());

        return ResponseEntity.ok(countriesFoundList);
    }

    @GetMapping(path = "/countries/{id}")
    public ResponseEntity<CountrySaveResponse> findCountryById(@Valid @NotNull @PathVariable("id") Long id){

        Country countryFound = countryService.findById(id);

        return ResponseEntity.ok(CountrySaveResponse.fromModel(countryFound));
    }

    @GetMapping(path = "/identification-types")
    public ResponseEntity<List<IdentificationTypeSaveResponse>> findAllIdentificationTypes(){

        List<IdentificationType> allIdentificationTypesFound = userService.findAllIdentificationTypes();

        List<IdentificationTypeSaveResponse> identificationTypesSaveResponse = allIdentificationTypesFound.stream().map(IdentificationTypeSaveResponse::fromModel)
                .collect(Collectors.toList());

        return ResponseEntity.ok(identificationTypesSaveResponse);
    }
}
