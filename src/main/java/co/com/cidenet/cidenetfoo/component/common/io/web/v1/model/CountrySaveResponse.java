package co.com.cidenet.cidenetfoo.component.common.io.web.v1.model;

import co.com.cidenet.cidenetfoo.component.common.model.Country;
import lombok.*;

@Data
@Generated
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CountrySaveResponse {

    private Long id;

    private String name;

    private String emailDomain;

    public static CountrySaveResponse fromModel(Country country){
        return CountrySaveResponse.builder().id(country.getId())
                .name(country.getName()).emailDomain(country.getEmailDomain())
                .build();
    }
}
