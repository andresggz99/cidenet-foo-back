package co.com.cidenet.cidenetfoo.component.user.service.model;

import lombok.*;

import java.time.LocalDate;

@Data
@Generated
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserQuerySearchCmd {

    private Long id;

    private String identification;

    private Long identificationTypeId;

    private Long laborCountryId;

    private Long divisionId;

    private String firstName;

    private String middleName;

    private String primaryLastName;

    private String secondLastName;

    private String email;

    private LocalDate enrollmentDate;

}
