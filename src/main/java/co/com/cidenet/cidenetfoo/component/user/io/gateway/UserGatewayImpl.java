package co.com.cidenet.cidenetfoo.component.user.io.gateway;

import co.com.cidenet.cidenetfoo.component.common.model.Country;
import co.com.cidenet.cidenetfoo.component.common.model.Division;
import co.com.cidenet.cidenetfoo.component.shared.web.exception.ResourceNotFoundException;
import co.com.cidenet.cidenetfoo.component.user.io.repository.IdentificationTypeRepository;
import co.com.cidenet.cidenetfoo.component.user.io.repository.UserRepository;
import co.com.cidenet.cidenetfoo.component.user.model.IdentificationType;
import co.com.cidenet.cidenetfoo.component.user.model.User;
import co.com.cidenet.cidenetfoo.component.user.service.UserGateway;
import co.com.cidenet.cidenetfoo.component.user.service.model.UserQuerySearchCmd;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.Predicate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import static java.util.Objects.nonNull;

@Repository
@RequiredArgsConstructor
public class UserGatewayImpl implements UserGateway {

    private final UserRepository userRepository;

    private final IdentificationTypeRepository identificationTypeRepository;

    private static final String USER_RESOURCE_NOT_FOUND = "User not found";

    private static final String IDENTIFICATION_TYPE_RESOURCE_NOT_FOUND = "Identification type not found";

    @Override
    public User save(User userToCreate) {

        final User userToBeCreated =
                userToCreate.toBuilder().createDate(LocalDateTime.now()).updateDate(LocalDateTime.now()).build();

        final User userCreated = userRepository.save(userToBeCreated);

        return userCreated;
    }

    @Override
    public User findById(Long id) {

        User userFound = userRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(USER_RESOURCE_NOT_FOUND));

        return userFound;
    }

    @Override
    public IdentificationType findIdentificationTypeById(Long id) {

        IdentificationType identificationTypeFound = identificationTypeRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(IDENTIFICATION_TYPE_RESOURCE_NOT_FOUND));

        return identificationTypeFound;
    }

    @Override
    public Optional<Long> getBiggestConsecutiveUsedInEmail(String email) {

        String emailWithoutNumbers = email.replaceAll("[0-9]", "").toLowerCase(Locale.ENGLISH);

        Optional<Long> biggestConsecutiveUsedInEmailFound = userRepository.getBiggestConsecutiveUsedInEmail(emailWithoutNumbers);

        return biggestConsecutiveUsedInEmailFound;
    }

    @Override
    public void deleteById(Long id) {

        findById(id);

        userRepository.deleteById(id);
    }

    @Override
    public User update(Long id, User userToUpdate) {

        final User userToBeUpdated =
                userToUpdate.toBuilder().updateDate(LocalDateTime.now()).build();

        final User userUpdated = userRepository.save(userToBeUpdated);

        return userUpdated;
    }

    @Override
    public Page<User> findByParameters(UserQuerySearchCmd queryCriteria, Pageable pageable) {

        Specification<User> specification = buildCriteria(queryCriteria);

        Page<User> resourcesFound = userRepository.findAll(specification, pageable);


        return resourcesFound;
    }

    @Override
    public List<IdentificationType> findAllIdentificationTypes() {

        List<IdentificationType> identificationTypes = identificationTypeRepository.findAll();

        return identificationTypes;
    }

    @Override
    public Boolean existsByEmail(String email) {

        Boolean emailAlreadyExists = userRepository.existsByEmail(email);

        return emailAlreadyExists;
    }

    private Specification<User> buildCriteria(UserQuerySearchCmd queryCriteria) {

        return (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();

            if (nonNull(queryCriteria.getId())) {
                predicates
                        .add(criteriaBuilder.and(
                                criteriaBuilder.equal(root.get("id"),  queryCriteria.getId())));
            }


            if (nonNull(queryCriteria.getFirstName())) {
                predicates
                        .add(criteriaBuilder.and(
                                criteriaBuilder.like(root.get("firstName"), "%" + queryCriteria.getFirstName() + "%")));
            }

            if (nonNull(queryCriteria.getEmail())) {
                predicates.add(criteriaBuilder
                        .and(criteriaBuilder.like(root.get("email"),
                                "%" + queryCriteria.getEmail() + "%")));
            }

            if (nonNull(queryCriteria.getMiddleName())) {
                predicates.add(criteriaBuilder
                        .and(criteriaBuilder.like(root.get("middleName"),
                                "%" + queryCriteria.getMiddleName() + "%")));
            }

            if (nonNull(queryCriteria.getPrimaryLastName())) {
                predicates.add(criteriaBuilder
                        .and(criteriaBuilder.like(root.get("primaryLastName"),
                                "%" + queryCriteria.getPrimaryLastName() + "%")));
            }

            if (nonNull(queryCriteria.getSecondLastName())) {
                predicates.add(criteriaBuilder
                        .and(criteriaBuilder.like(root.get("secondLastName"),
                                "%" + queryCriteria.getSecondLastName() + "%")));
            }

            if (nonNull(queryCriteria.getEnrollmentDate())) {
                predicates.add(criteriaBuilder
                        .and(criteriaBuilder.equal(root.get("enrollmentDate"),
                                 queryCriteria.getEnrollmentDate())));
            }

            if (nonNull(queryCriteria.getIdentificationTypeId())) {
                predicates.add(criteriaBuilder
                        .and(criteriaBuilder.equal(root.get("identificationType"),
                                 IdentificationType.builder().id(queryCriteria.getIdentificationTypeId()).build())));
            }

            if (nonNull(queryCriteria.getLaborCountryId())) {
                predicates.add(criteriaBuilder
                        .and(criteriaBuilder.equal(root.get("laborCountry"),
                                 Country.builder().id(queryCriteria.getLaborCountryId()).build())));
            }

            if (nonNull(queryCriteria.getDivisionId())) {
                predicates.add(criteriaBuilder
                        .and(criteriaBuilder.equal(root.get("division"),
                                 Division.builder().id(queryCriteria.getDivisionId()).build())));
            }

            return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }
}
