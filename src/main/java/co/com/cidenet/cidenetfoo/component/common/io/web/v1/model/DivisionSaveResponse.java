package co.com.cidenet.cidenetfoo.component.common.io.web.v1.model;

import co.com.cidenet.cidenetfoo.component.common.model.Division;
import lombok.*;

@Data
@Generated
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class DivisionSaveResponse {

    private Long id;

    private String name;

    public static DivisionSaveResponse fromModel(Division division){
        return DivisionSaveResponse.builder().id(division.getId()).name(division.getName())
                .build();
    }
}
