package co.com.cidenet.cidenetfoo.component.user.io.web.v1.model;

import lombok.*;

@Data
@Generated
@NoArgsConstructor
@AllArgsConstructor
@Builder
class DivisionSaveResponse {

    private Long id;

    private String name;
}
