package co.com.cidenet.cidenetfoo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CidenetFooBackApplication {

	public static void main(String[] args) {
		SpringApplication.run(CidenetFooBackApplication.class, args);
	}

}
